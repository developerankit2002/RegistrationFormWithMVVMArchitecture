package com.example.registrationformapplication.viewModal
import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.obserable.SignUpFieldObserable
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.utility.Keys

class SignUpSharedPrefRepositoryViewModel(private val signupsharedPrefRepo: SignUpSharedPrefRepository,private val context : Context):ViewModel() {
    private var firstName: String=" "
    private var lastname: String=" "
    private var mobileNumber: String=" "
    private var loginNumber: String=" "

    var signupfieldObserabl: SignUpFieldObserable = SignUpFieldObserable()

    //------------------Data Save in Repository-----------------------------------------------------


    fun saveData():Boolean {
        //-------------------Data Get From Base Obserable Class Fields------------------------------
        firstName = signupfieldObserabl.fname
        lastname = signupfieldObserabl.lname
        mobileNumber = signupfieldObserabl.mnumber

        if (firstName.isEmpty() && lastname.isEmpty() && mobileNumber.isEmpty()) {
            Toast.makeText(context, " All Field is Empty ", Toast.LENGTH_SHORT).show()
        }
        if (firstName.isEmpty()){
            Toast.makeText(context, " First Name Can't be Empty ", Toast.LENGTH_SHORT).show()
        }
        if (lastname.isEmpty()){
            Toast.makeText(context, " Last Name Can't be Empty ", Toast.LENGTH_SHORT).show()
        }
        val condition : Boolean = if (mobileNumber.length!=10){
            Toast.makeText(context, " Enter Mobile Number Valid ", Toast.LENGTH_SHORT).show()
            false
        } else {

            signupsharedPrefRepo.getPreference(context).edit()
                .putString(Keys.FNAME, firstName).apply()
            signupsharedPrefRepo.getPreference(context).edit()
                .putString(Keys.LNAME, lastname).apply()
            signupsharedPrefRepo.getPreference(context).edit()
                .putString(Keys.MOBNO, mobileNumber).apply()
            true
        }
        return condition
    }

    //-----------------Data Get From Repository-----------------------------------------------------
    fun getData(MobileNumber: String):Boolean {
        loginNumber=MobileNumber
        mobileNumber = signupsharedPrefRepo.getPreference(context).getString(Keys.MOBNO, "").toString()
        return if (loginNumber == mobileNumber){
            true
        } else{
            Toast.makeText(context, "This Number is Not Available in Database ", Toast.LENGTH_SHORT).show()
            false
        }
   }

}

package com.example.registrationformapplication.obserable

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class FieldObserable:BaseObservable() {
    @get:Bindable
    var fname:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.fname)
    }

    @get:Bindable
    var lname:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.lname)
    }

    @get:Bindable
    var mobilenumber:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.mobilenumber)
    }

    @get:Bindable
    var altnumber:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.altnumber)
    }

    @get:Bindable
    var dob:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.dob)
    }


    @get:Bindable
    var email:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.email)
    }


    @get:Bindable
    var pannumber:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.pannumber)
    }



    @get:Bindable
    var adharcard:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.adharcard)
    }


    @get:Bindable
    var address:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.address)
    }


}
package com.example.registrationformapplication.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityFormBinding
import com.example.registrationformapplication.obserable.FieldObserable
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.FormActivityviewModal

class FormActivity : AppCompatActivity(), View.OnClickListener, RadioGroup.OnCheckedChangeListener,
    CompoundButton.OnCheckedChangeListener {
    private lateinit var binding:ActivityFormBinding
    private lateinit var viewModal : FormActivityviewModal
    private lateinit var fieldobserver : FieldObserable
    private var gender:String=""
    private var arrayList=ArrayList<String>()
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
         binding=DataBindingUtil.setContentView(this,R.layout.activity_form)
        viewModal=ViewModelProvider(this)[FormActivityviewModal::class.java]

         fieldobserver= FieldObserable()
        binding.myviewModaldata = viewModal
        //------------------------------------------------------------------------------------------
        binding.lifecycleOwner = this
        //------------------------------------------------------------------------------------------

        viewModal.msg.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }


        binding.btnsubmit.setOnClickListener(this)
        binding.rbGroup.setOnCheckedChangeListener(this)
        binding.cbProgramming.setOnCheckedChangeListener(this)
        binding.cbRunbusiness.setOnCheckedChangeListener(this)
        binding.cbTravling.setOnCheckedChangeListener(this)
        binding.cbPlayingCricket.setOnCheckedChangeListener(this)
        binding.cbReadingBook.setOnCheckedChangeListener(this)

        val intent=intent
        val usertype= intent?.getStringExtra(Keys.USER_TYPE).toString()
    }

    override fun onClick(v : View?) {
        viewModal.datavalidation(gender, arrayList)
    }

    override fun onCheckedChanged(group : RadioGroup?, checkedId : Int) {
        when(group?.checkedRadioButtonId){
            R.id.rb_male ->{
                gender=binding.rbMale.text.toString()
            }

            R.id.rb_female ->{
                gender=binding.rbFemale.text.toString()
            }

            R.id.rb_custom ->{
                gender=binding.rbCustom.text.toString()
            }

        }
    }

    override fun onCheckedChanged(buttonView : CompoundButton?, isChecked : Boolean) {
        when(buttonView?.id){
            R.id.cb_programming ->{
                if(binding.cbProgramming.isChecked){
                    arrayList.add(binding.cbProgramming.text.toString())

                }
                else
                {
                    arrayList.remove(binding.cbProgramming.text.toString())
                }

            }
            R.id.cb_runbusiness ->{
                if(binding.cbRunbusiness.isChecked){
                    arrayList.add(binding.cbRunbusiness.text.toString())

                }
                else
                {
                    arrayList.remove(binding.cbRunbusiness.text.toString())

                }

            }
            R.id.cb_travling ->{
                if(binding.cbTravling.isChecked){
                    arrayList.add(binding.cbTravling.text.toString())

                }
                else
                {
                    arrayList.remove(binding.cbTravling.text.toString())

                }

            }
            R.id.cb_playingCricket ->{
                if (binding.cbPlayingCricket.isChecked) {
                    arrayList.add(binding.cbPlayingCricket.text.toString())

                } else {
                    arrayList.remove(binding.cbPlayingCricket.text.toString())


                }

            }
            R.id.cb_readingBook ->{
                if(binding.cbReadingBook.isChecked){
                    arrayList.add(binding.cbReadingBook.text.toString())

                }
                else
                {
                    arrayList.remove(binding.cbReadingBook.text.toString())

                }

            }
        }
    }


}
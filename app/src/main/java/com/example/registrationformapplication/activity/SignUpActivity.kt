package com.example.registrationformapplication.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivitySignUpBinding
import com.example.registrationformapplication.factory.SignUpSharedPrefRepositoryViewModalFactory
import com.example.registrationformapplication.obserable.SignUpFieldObserable
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.SignUpSharedPrefRepositoryViewModel

class SignUpActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {
    private lateinit var binding:ActivitySignUpBinding
    private lateinit var viewModel:SignUpSharedPrefRepositoryViewModel
    private lateinit var factory:SignUpSharedPrefRepositoryViewModalFactory
    private lateinit var signUpFieldObserable : SignUpFieldObserable

    private val data = arrayOf("Admin", "HR", "Consultant", "Faculty")
    private var spinnerdata:String? = ""

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        //------------------------------------------------------------------------------------------
        binding=DataBindingUtil.setContentView(this,R.layout.activity_sign_up)
        //------------------------------------------------------------------------------------------
        factory= SignUpSharedPrefRepositoryViewModalFactory(SignUpSharedPrefRepository,this)
        //------------------------------------------------------------------------------------------
        viewModel=ViewModelProvider(this,factory)[SignUpSharedPrefRepositoryViewModel::class.java]
        //------------------------------------------------------------------------------------------
        SignUpFieldObserable().also { signUpFieldObserable = it }
        //------------------------------------------------------------------------------------------
        binding.mydataview =viewModel
        //------------------------------------------------------------------------------------------
        binding.lifecycleOwner = this
        //------------------------------------------------------------------------------------------
        binding.btnSignup.setOnClickListener(this)
        //------------------------------------------------------------------------------------------
        binding.tvLoginhere.setOnClickListener {
            val intent=Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }
        //------------------------------------------------------------------------------------------
        binding.tvSkip.setOnClickListener {
            val intent=Intent(this,FormActivity::class.java)
            intent.putExtra(Keys.USER_TYPE,"Guest")
            startActivity(intent)
        }
        //-----------------------------------------------------------------------------------------

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, data)
        binding.spinner.adapter = dataAdapter
        binding.spinner.onItemSelectedListener=this

    }

    override fun onClick(v : View?) {
        if (viewModel.saveData()){
        val intent=Intent(this,FormActivity::class.java)
        intent.putExtra(Keys.USER_TYPE,spinnerdata)
        startActivity(intent)
        }else{
            Toast.makeText(this, " Something! went Wrong! ", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onItemSelected(adapter : AdapterView<*>?, view : View?, position : Int, id : Long) {
        spinnerdata=adapter?.getItemAtPosition(position).toString()
    }

    override fun onNothingSelected(parent : AdapterView<*>?) {
        TODO("Not yet implemented")
    }

}